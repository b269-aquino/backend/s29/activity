// Insert Documents
db.users.insertMany([
	{
        firstName: "Stephen",
        lastName: "Hawking",
        age: 76,
        contact: {
            phone: "87654321",
            email: "stephenhawking@gmail.com"
        },
        courses: [ "Python", "React", "PHP" ],
        department: "HR"
    },
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "HR"
    },
    {
    	firstName: "Jane",
    	lastName: "Doe",
    	age: 21,
    	contact: {
    		phone: "87654321",
    		email: "janedoe@gmail.com"
    	},
    	courses: ["CSS", "JavaScript", "Python"],
    	department: "HR"
    },
    {
		firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
			phone: "12345678",
			email: "bill@gmail.com"
		},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations",
		status: "active"
	},
]);

// Insert embedded array
db.users.insertOne({
	namearr: [
		{
			namea: "juan"
		},
		{
			nameb: "tamad"
		}
	]
});

// find users with letter s in their first name or d in their last name
db.users.find({
	$or: 
	[
		{firstName: {$regex: "S", $options: '$i'}},
		{lastName: {$regex: "D", $options: '$i'}}
	]
},
	{
		firstName:1,
		lastName:1,
		_id: 0
	}
);

// find users who are from the HR department and their age is greater than or equal to 70.

db.users.find({$and: [{department: "HR"}, {age: {$gte: 70 }}]})


// find users with the letter e in their first name and has an age of less than or equal to 30
db.users.find({
	$and: 
	[
		{firstName: {$regex: "e", $options: '$i'}},
		{age: {$lte: 70}}
	]
});